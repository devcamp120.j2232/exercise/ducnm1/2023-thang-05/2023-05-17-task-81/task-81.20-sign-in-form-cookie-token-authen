class CustomModal{
    constructor(title){
        this.title = title;
    }

    setTitle(title){
        this.title = title;
    }

    setContent(content){
        this.content = content;
    }
}