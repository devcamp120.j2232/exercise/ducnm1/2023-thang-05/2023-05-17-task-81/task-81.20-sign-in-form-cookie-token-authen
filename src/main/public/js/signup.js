import User from "./user.js";
import ValidatonMessage from "./ValidationMesssage.js";

$(document).ready(function () {
    addEventListener();
});

function addEventListener() {
    $("#btn-signup").on("click", function () {
        onBtnSignUpClick();
    });
}

function onBtnSignUpClick() {
    let user = new User();
    user.username = $("#username").val().trim();
    user.email = $("#email").val().trim();
    user.password = $("#password").val();
    user.confirmPassword = $("#confirm-password").val();
    user.acceptPolicy = $("#check").prop("checked");

    let validatonMessage = user.validateUser();
    if (validatonMessage.result == false) {
        console.log(validatonMessage);
        alert(validatonMessage.message);
    }
    else {
        user.signUp();
    }
}



