import User from "./user.js";
import ValidatonMessage from "./ValidationMesssage.js";

$(document).ready(function () {
    addEventListener();
});

function addEventListener() {
    $("#btn-login").on("click", function () {
        onBtnLoginClick();
    });

    $("#btn-logout").on("click", function () {
        onBtnLogoutClick();
    });

    $("#btn-info").on("click", function () {
        onBtnInfoClick();
    });
}

function onBtnLoginClick() {
    console.log("đăng nhập");
    let user = new User();
    user.username = $("#username").val().trim();
    user.password = $("#password").val();

    user.signIn();
}

function onBtnLogoutClick(){
    console.log("đăng xuất");
    let user = new User();
    user.signOut();
}

function onBtnInfoClick(){
    console.log("lấy thông tin");
    let user = new User();
    user.getInfo();
}
