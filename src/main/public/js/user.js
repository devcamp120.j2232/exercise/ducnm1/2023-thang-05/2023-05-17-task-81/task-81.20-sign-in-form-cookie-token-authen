import ValidatonMessage from "../js/ValidationMesssage.js";

export default class User {
    constructor(username, email, password, confirmPassword, acceptPolicy) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.acceptPolicy = acceptPolicy;
    }

    validateUser() {
        if (!this.username) {
            return new ValidatonMessage("Username cannot be empty", false);
        }

        if (!this.email) {
            return new ValidatonMessage("Email cannot be empty", false);
        }
        if (!this.password) {
            return new ValidatonMessage("Password cannot be empty", false);
        }
        if (!this.confirmPassword) {
            return new ValidatonMessage(
                "Confirm password cannot be empty",
                false
            );
        }

        if (this.password !== this.confirmPassword) {
            return new ValidatonMessage("Password does not match", false);
        }

        if (!this.acceptPolicy) {
            return new ValidatonMessage(
                "Please accept terms of use & privacy policy",
                false
            );
        }

        return new ValidatonMessage("", true);
    }

    signUp() {
        let userRequest = {
            username: this.username,
            email: this.email,
            password: this.password,
        };

        fetch("http://localhost:8080/api/auth/signup", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(userRequest),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    signIn() {
        let userRequest = {
            username: this.username,
            password: this.password,
        };

        fetch("http://localhost:8080/api/auth/signin", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(userRequest),
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                setCookie("token", data.accessToken, 1);
            })
            .catch(function (error) {
                console.error(error);
            });
    }

    signOut() {
        let cookie = getCookie("token");
        console.log(cookie);
    }

    getInfo() {
    }
}

function setCookie(name, value, days) {
    const expires = new Date();
    expires.setTime(expires.getTime() + days * 24 * 60 * 60 * 1000);
    document.cookie = `${name}=${value};expires=${expires.toUTCString()};path=/`;
    console.log("document.cookie");
    console.log(document.cookie);
}

function getCookie(name) {
    const cookies = document.cookie.split("; ");
    for (let cookie of cookies) {
        const [key, value] = cookie.split("=");
        if (key === name) {
            return value;
        }
    }
    return null;
}